## Macros

### Heal

| spell | macro |
| --- | --- |
| Abolish Disease | #showtooltip /dismount; /cast [@mouseover,help,nodead][help,nodead][@player] Abolish Disease |
| Dispel Magic | #showtooltip /dismount; /cast [@mouseover,help,nodead] dispel magic; [harm,nodead] [exists,nodead][@player] dispel magic |
| Greater Heal (2) | #showtooltip /dismount; /cancelaura Shadowform; /cast [@mouseover,help,nodead][help,nodead][@player] Greater Heal (Rank 2) |
| Greater Heal | #showtooltip /dismount; /cancelaura Shadowform; /cast [@mouseover,help,nodead][help,nodead][@player] Greater Heal |
| Heal (2) | #showtooltip /dismount; /cancelaura Shadowform; /cast [@mouseover,help,nodead][help,nodead][@player] Heal(Rank 2) |
| Heal | #showtooltip /dismount; /cancelaura Shadowform; /cast [@mouseover,help,nodead][help,nodead][@player] Heal |
| Flash Heal (3) | #showtooltip /dismount; /cancelaura Shadowform; /cast [@mouseover,help,nodead][help,nodead][@player] Flash Heal(Rank 3) |
| Flash Heal | #showtooltip /dismount; /cancelaura Shadowform; /cast [@mouseover,help,nodead][help,nodead][@player] Flash Heal |
| Renew | #showtooltip /dismount; /cancelaura Shadowform; /cast [@mouseover,help,nodead][help,nodead][@player] Renew |
| Shield | #showtooltip /dismount; /cast [@mouseover,help,nodead][help,nodead][@player] Power Word: Shield |
| Holy Nova | #showtooltip /dismount; /cancelaura Shadowform; /cast Holy Nova |
| Prayer of Healing (2) | #showtooltip /dismount; /cancelaura Shadowform; /cast Prayer of Healing(Rank 2) |
| Prayer of Healing | #showtooltip /dismount; /cancelaura Shadowform; /cast Prayer of Healing |
| Inner Focus Greater Heal | #showtooltip /dismount; /cancelaura Shadowform; /cast Inner Focus /cast [@mouseover,help,nodead][help,nodead][@player] Greater Heal |
| Inner Focus Prayer of Heal | #showtooltip /dismount; /cancelaura Shadowform; /cast Inner Focus /cast Prayer of Healing |
| Resurrection | #showtooltip /dismount; /cancelaura Shadowform; /cast [@mouseover,help,nodead][help,nodead][@player] Heal |

### Buffs

| spell | macro |
| --- | --- |
| Divine Spirit | #showtooltip /dismount; /cast [@mouseover,help,nodead][help,nodead][@player] Divine Spirit |
| Power Word: Fort | #showtooltip /dismount; /cast [@mouseover,help,nodead][help,nodead][@player] Power Word: Fortitude |
| Shadow Protection | #showtooltip /dismount; /cast [@mouseover,help,nodead][help,nodead][@player] Shadow Protection |
| Fear Ward | #showtooltip /dismount; /cast [@mouseover,help,nodead][help,nodead][@player] Fear Ward |
| Levitate | #showtooltip /dismount; /cast [@mouseover,help,nodead][help,nodead][@player] Levitate |
| Inner Fire | #showtooltip /dismount; /cast Inner Fire |
| Stone Form | #showtooltip /dismount; /cast Stone Form |
| Prayer Fortitude | #showtooltip /dismount; /cast Prayer of Fortitude |
| Prayer Shadow | #showtooltip /dismount; /cast Prayer of Shadow Protection |
| Prayer Spirit | #showtooltip /dismount; /cast Prayer of Spirit |

### CC

| spell | macro |
| --- | --- |
| Psychic Scream | #showtooltip /dismount; /cast Psychic Scream |
| Mind Control | #showtooltip /dismount; /cast [@mouseover,harm,nodead] [@target] Mind Control |

### Damage

| spell | macro |
| --- | --- |
| Holy Fire | #showtooltip /dismount; /cast [@mouseover,harm,nodead] [@target] Holy Fire |
| Smite | #showtooltip /dismount; /cast [@mouseover,harm,nodead] [@target] Smite |
| Mind Blast | #showtooltip /dismount; /cast [@mouseover,harm,nodead] [@target] Mind Blast |
| Mind Flay | #showtooltip /dismount; /cast [nochanneling][@mouseover,harm, nodead] [@target]  Mind Flay |
| SW: Pain | #showtooltip /dismount; /cast [@mouseover,harm,nodead] [@target] Shadow Word: Pain |
| Wand | /cast !shoot |

## Addons

+ Weak Auras2
  + [Clickable Rebuffs](https://wago.io/7GKuA1thk)
  + [Jt's Bars](https://wago.io/02-5Z0y0U): Swing and 5s timer
  + [Parla WA](https://wago.io/g7zquYZ82): Tracks Pot CD, Grenade CD, Healthstone CD - Spirit Tap - Weakened Soul - Fearward CD - Desperate Prayer CD - Inner Fire Duration + Stacks
+ TODO

## Tailoring

[https://classic.wowhead.com/guides/tailoring-classic-wow-1-300](https://classic.wowhead.com/guides/tailoring-classic-wow-1-300)

### Shopping List

+ 120 Linen Cloth
+ 165 Wool Cloth
+ 660 Silk Cloth
+ 650 Mageweave Cloth
+ 35 Runecloth

### Recipes

+ Mooncloth ([Qia](https://classic.wowhead.com/npc=11189/qia) at 61,37 in Winterspring)
+ Runecloth Bag (rare sale from [Qia](https://classic.wowhead.com/npc=11189/qia) at 61,37 in Winterspring)
+ Runecloth Gloves (rare sale from [Qia](https://classic.wowhead.com/npc=11189/qia) at 61,37 in Winterspring)
+ Runecloth Cloak rare at Darnall in Moonglade @ the house north east 52,33
+ Runecloth Boots rare at Darnall in Moonglade @ the house north east 52,33
+ Runecloth Robe rare at Darnall in Moonglade @ the house north east 52,33

### Leveling

#### Linen

+ 60 Blot of Linen Cloth (40)
+ Linen Belt until (50)
+ Learn Journeyman Tailor
+ 10 Heavy Linen Gloves (60)
+ 15 Reinforced Linen Cape (75)

#### Wool

+ 55 Bolt of Wool Cloth (105)
+ 5 Green Woolen Vest (110)
+ 15 Double-stitched Woolen Shoulders (125)
+ Learn Expert Tailor

#### Silk

+ 85 Bolt of Silk Cloth (145)
+ 10 Azure Silk Hood (155)
+ 5 Azure Silk Pants (160)
+ 10 Silk Headband (170)
+ 5 Formal White Shirt (175)

#### Mageweave (and some Silk)

+ 130 Bolt of Mageweave (185)
+ 20 Crimson Silk Vest (205)
+ 15 Black Mageweave Vest/Leggins (220) 30B/150C
+ 5 Black Mageweave Gloves (225) 10B/50C

#### Artisan Tailoring

+ take all the stuff to Theramore
+ Learn Artisan Tailoring [Timothy Worthington](https://classic.wowhead.com/npc=11052/timothy-worthington)
+ 5 Black Mageweave Gloves (230) 10B/50C
+ 10 Mageweave Bags (240) 40B/200C (keep or sell)
+ 10 Red Mageweave Bags (250) 40B/200C (keep or sell)

#### Runecloth

+ 5 Bolt of Runecloth (255) 35C
+ Buy Runecloth Belt from Timothy and go back to Ironforge

#### Way to 300 Mooncloth

+ Bolt of Runecloth until 260
+ Get Mooncloth recipe level till 290 or even 300

#### 300 Runecloth items

+ [Runecloth Robe](https://classic.wowhead.com/spell=18406) @260
+ [Runecloth Tunic](https://classic.wowhead.com/spell=18407) @260
+ [Runecloth Cloak](https://classic.wowhead.com/spell=18409) @ 265
+ [Runecloth Gloves](https://classic.wowhead.com/spell=18417) @ 275
+ [Runecloth Boots](https://classic.wowhead.com/spell=18423) @ 280
+ [Runecloth Pants](https://classic.wowhead.com/spell=18438) @285
+ [Runecloth Headband](https://classic.wowhead.com/spell=18444) @295

## Enigneering

### Shopping List

+ Blacksmith Hammer
+ 40 Rough Stone
+ 31 Copper Bar

### Levelling

+ Rough Blasting Powder (30)
+ ~25 Handful of Copper Bolts (50)
+ Learn Journeyman Engineer
+ Arclight Spanner (51)
